from time import time
from fastapi import FastAPI
from binance import Client
import datetime

app = FastAPI()



@app.get("/{symbol}/{duration}")
async def root(symbol: str, duration: str):

    # using API from Binance
    api_key = '660q8PDVBaUS5luGX3rUe3RteDOvek7WJyWcq4c3rDjdXfrsR33rc3EVQLAlPNtx'
    api_secret = 'VCGo9OgZw9V6KMCIGG09Rlt6ntINGCNAPE1o2xtfr6WsZlWU3laDNFZphC9zWd8j' 
    client = Client(api_key, api_secret)


   
    # check duration parameter
    if duration == '1m':
        check_type = 'm'
        step = 1
        interval = Client.KLINE_INTERVAL_1MINUTE
    elif duration == '3m':
        check_type = 'm'
        step = 3
        interval = Client.KLINE_INTERVAL_3MINUTE
    elif duration == '5m':
        check_type = 'm'
        step = 5
        interval = Client.KLINE_INTERVAL_5MINUTE
    elif duration == '15m':
        check_type = 'm'
        step = 15
        interval = Client.KLINE_INTERVAL_15MINUTE
    elif duration == '30m':
        check_type = 'm'
        step = 30
        interval = Client.KLINE_INTERVAL_30MINUTE
    elif duration == '1h':
        check_type = 'h'
        step = 1
        interval = Client.KLINE_INTERVAL_1HOUR
    elif duration == '2h':
        check_type = 'h'
        step = 2
        interval = Client.KLINE_INTERVAL_2HOUR
    elif duration == '4h':
        check_type = 'h'
        step = 4
        interval = Client.KLINE_INTERVAL_4HOUR
    elif duration == '6h':
        check_type = 'h'
        step = 6
        interval = Client.KLINE_INTERVAL_6HOUR
    elif duration == '8h':
        check_type = 'h'
        step = 8
        interval = Client.KLINE_INTERVAL_8HOUR
    elif duration == '12h':
        check_type = 'h'
        step = 12
        interval = Client.KLINE_INTERVAL_12HOUR
    elif duration == '1d':
        check_type = 'd'
        step = 1
        interval = Client.KLINE_INTERVAL_1DAY
    elif duration == '3d':
        check_type = 'd'
        step = 3
        interval = Client.KLINE_INTERVAL_3DAY
    elif duration == '1w':
        check_type = 'w'
        step = 1
        interval = Client.KLINE_INTERVAL_1WEEK

    # get data from API
    klines = client.get_historical_klines(symbol, interval, '1 week ago UTC')

    # to assign number to each candle
    name_index = 1 

    # return dict
    ret = {}


    # get the first candle's time
    time_start = datetime.datetime.now()-datetime.timedelta(days=7)
    if check_type=='m':

        for each_candle in klines:
            
            
             # store data in ret
            ret['candle_'+str(name_index)] ={ 'date': time_start+datetime.timedelta(minutes=step),'open': each_candle[1], 'high': each_candle[2], 'low' : each_candle[3], 'close': each_candle[4] }
            
            # update
            name_index=name_index+1
            time_start=time_start+datetime.timedelta(minutes=step)
    elif check_type=='h':

        for each_candle in klines:
            
            ret['candle_'+str(name_index)] ={ 'date': time_start+datetime.timedelta(hours=step),'open': each_candle[1], 'high': each_candle[2], 'low' : each_candle[3], 'close': each_candle[4] }
            name_index=name_index+1
            time_start=time_start+datetime.timedelta(hours=step)
    elif check_type=='d':

        for each_candle in klines:
            
            ret['candle_'+str(name_index)] ={ 'date': time_start+datetime.timedelta(days=step),'open': each_candle[1], 'high': each_candle[2], 'low' : each_candle[3], 'close': each_candle[4] }
            name_index=name_index+1
            time_start=time_start+datetime.timedelta(days=step)
    elif check_type=='w':

        for each_candle in klines:
            
            ret['candle_'+str(name_index)] ={ 'date': time_start+datetime.timedelta(weeks=1),'open': each_candle[1], 'high': each_candle[2], 'low' : each_candle[3], 'close': each_candle[4] }
            name_index=name_index+1
            


    
    
    return {'data': ret }
