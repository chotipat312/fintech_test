# Fintech_Test

This API take input timeframe and cryptocurrency pairs, and then fetch historical data from Binance exchange 1 weak ago to retern open/high/low/close price.

# How to use this 

  GET /{symbol}/{duration}

## Parameters
    symbol : string --> input cryptocurrency pair. You can see all supported symbols in 'symbol.txt'

    duration : string --> input timeframe that you want from "1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 8h, 12h, 1d, 3d, 1w"

## Return 
    return a JSON containing each candle's 
    date : date&time  
    open price : int 
    high price : int 
    low price : int 
    close price : int

### Example 
    want to know BNB / ETH price, timeframe daily (1 day per candle)

#### URL
    http://127.0.0.1:8000/BNBETH/1d
#### Result
    {"data":{"candle_1":{"date":"2021-09-11T21:59:01.818801","open":"0.12500000","high":"0.12580000","low":"0.12360000","close":"0.12400000"},"candle_2":{"date":"2021-09-12T21:59:01.818801","open":"0.12390000","high":"0.12410000","low":"0.12040000","close":"0.12230000"},"candle_3":{"date":"2021-09-13T21:59:01.818801","open":"0.12230000","high":"0.12440000","low":"0.12110000","close":"0.12140000"},"candle_4":{"date":"2021-09-14T21:59:01.818801","open":"0.12150000","high":"0.12200000","low":"0.12000000","close":"0.12070000"},"candle_5":{"date":"2021-09-15T21:59:01.818801","open":"0.12070000","high":"0.12640000","low":"0.11930000","close":"0.11930000"},"candle_6":{"date":"2021-09-16T21:59:01.818801","open":"0.11940000","high":"0.12010000","low":"0.11730000","close":"0.11880000"},"candle_7":{"date":"2021-09-17T21:59:01.818801","open":"0.11890000","high":"0.11950000","low":"0.11750000","close":"0.11850000"}}}

    

